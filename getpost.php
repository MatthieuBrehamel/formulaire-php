<?php
//    !!  dans cette page on ne génère aucun affichage  !!
//configuration de la session : nom donné au cookie de session
session_name("TP4");
//démarrage du gestionnaire de session, qui permet d'accéder au tableau $_SESSION
session_start();
$nberreurs=0;


//paramètres PDORow
$host="localhost";
$base="tp3";
$port=3306;
$login="root";
$passe="password";

//mise en place des erreurs PDO : gestion des exceptions
function exception_handler_perso($exception) {
	print "Une exception a &eacute;t&eacute; lanc&eacute;e !!!";
	print "<pre>";
	var_dump($exception);
	print "</pre>";
}
set_exception_handler('exception_handler_perso');

$db = new PDO("mysql:host=$host;port=$port;dbname=$base", $login, $passe);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->query("SET NAMES UTF8");


if(empty($_POST['nom'])){
	$erreurs['nom'] = "Le nom de groupe est obligatoire";
	$nberreurs++;
}
else {
	$groupe= $db->prepare("SELECT * from ca where nom = ?");
	$groupe->execute(array($_POST['nom']));
	if($groupe->rowCount()>0){
		$erreurs['nom'] = "Ce nom de groupe existe déjà";
		$nberreurs++;
	}
}


if(empty($_POST['email'])){
	$erreurs['email'] = "l'adresse mail est vide";
	$nberreurs++;
}
else {
	if(filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)===false){
		$erreurs['email'] = "l'adresse mail est invalide";
		$nberreurs++;
	}else{
		$email= $db->prepare("SELECT * from ca where adresseEmail = ?");
		$email->execute(array($_POST['email']));
		if($email->rowCount()>0){
			$erreurs['email'] = "Cette adresse email est déjà utilisée";
			$nberreurs++;
		}


	}
}

if(empty($_POST['pass'])){
	$erreurs['pass']="le mot de passe est vide";
	$nberreurs++;
}
else {
	if(strlen($_POST['pass'])<8){
		$erreurs['pass']="mot de passe trop court";
		$nberreurs++;
	}
}

if(empty($_POST['annee'])){
	$erreurs['annee']="l'année est vide";
	$nberreurs++;
}
else {

	$options = array(
	    'options' => array(
	        'default' => date('Y'),
	        'min_range' => 1900,
					'max_range' => date('Y')

	    )
	);

	if(filter_var($_POST['annee'], FILTER_VALIDATE_INT, $options)===false){
		$erreurs['annee']="la date est invalide";
		$nberreurs++;
	}
}


if(empty($_POST['web'])){
	$erreurs['web']="le site web est vide";
	$nberreurs++;
}
else {
	if(filter_var($_POST['web'],FILTER_VALIDATE_URL)===false){
		$erreurs['web']= "url invalide";
		$nberreurs++;
	}
}

if(empty($_POST['Dept'])){
	$erreurs['Dept']="Il faut choisir un département";
	$nberreurs++;
}


if(!isset($_POST['scene'])){
	$erreurs['scene']="Il faut choisir une scene";
	$nberreurs++;
}

if(empty(trim($_POST['presentation']))){
	$erreurs['presentation']="Décrivez votre groupe";
	$nberreurs++;
}



if(!isset($_POST['Accepte'])){
	$erreurs['Accepte']="Il faut accepter les conditions";
	$nberreurs++;
}


if($nberreurs>0){
//on stocke les erreurs dans la session
$_SESSION['messages']=$erreurs;

//on stocke les champs passés en POST dans la session
$_SESSION['champs']=$_POST;

//on envoie au navigateur une commande de redirection vers la page de formulaire
header("Location:TP4.php");
}
else {
	//on insère dans la base de données les valeurs

	$insertion = $db->prepare("INSERT INTO ca(Nom, adresseEmail, motdepasse, departement, typescene, anneecreation, presentation, siteweb)
															VALUES (:Nom, :email, :pass, :departement, :typescene, :annee, :presentation, :siteweb)");
	$insertion->execute(array(
		":Nom"				=> $_POST['nom'],
		":email"			=>$_POST['email'],
		":pass"				=>password_hash($_POST['pass'],PASSWORD_DEFAULT),//on chiffre le mot de passe
		":departement"=>$_POST['Dept'],
		":typescene"	=>$_POST['scene'],
		":annee"			=>$_POST['annee'],
		":presentation"=>$_POST['presentation'],
		":siteweb"		=>$_POST['web']

	));

	//on vide le tableau de session (erreurs, valeurs)
	$_SESSION=array();

	//on envoie au navigateur une commande de redirection vers la page de confirmation
	header("Location:OK.php");


}

 ?>
