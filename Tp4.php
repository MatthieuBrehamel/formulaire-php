<?php
//configuration de la session : nom donné au cookie de session
session_name("TP4");
//démarrage du gestionnaire de session, qui permet d'accéder au tableau $_SESSION
session_start();

//fonction outil pour afficher une erreur si un champ est enregistré comme invalide
function affiche_erreur($champ){
	if(isset($_SESSION['messages'][$champ]))
		echo "<p class='erreur'>".$_SESSION['messages'][$champ].'</p>';
}

//fonction outil pour récupérer un champ précédemment saisi
//on filtre la valeur saisie pour que les ' et " éventuels ne gênent pas
function valeur($champ){
	if(isset($_SESSION['champs'][$champ]))
		 return filter_var ($_SESSION['champs'][$champ],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	else {
			return '';
	}
}


$host="localhost";
$base="tp3";
$port=3306;
$login="root";
$passe="password";


//mise en place des erreurs PDO : gestion des exceptions
function exception_handler_perso($exception) {
	print "Une exception a &eacute;t&eacute; lanc&eacute;e !!!";
	print "<pre>";
	var_dump($exception);
	print "</pre>";
}

set_exception_handler('exception_handler_perso');

$db = new PDO("mysql:host=$host;port=$port;dbname=$base", $login, $passe);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->query("SET NAMES UTF8");

$depts= $db->query("SELECT * from Departement");
$scenes = $db->query("SELECT * from Scene");
?>
<html>
<head>
<link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/pure-min.css" integrity="sha384-nn4HPE8lTHyVtfCBi5yW9d20FjT8BJwUXyWZT9InLYax14RDjBj46LmSztkmNP9w" crossorigin="anonymous">
<style>

.erreur{
	background:#FEE;

}

input:invalid+span:after {
  content: '✖';
  padding-left: 5px;
}

input:valid+span:after {
  content: '✓';
  padding-left: 5px;
}
.container{
	width:50%;
	margin: auto;
}
</style>
</head>
<body>
	<div class='pure-g  container'>
			<div class='pure-u-1'>
<?php
//on vérifie si des messages sont présents dans le tableau de session
//et on informe l'utilisateur
if(isset($_SESSION['messages']))
	echo "<p class='erreur'> ✖ Il manque des informations </p>";
 ?>
<form class='pure-form pure-form-aligned' action="getpost.php" method='POST'>
	<?php affiche_erreur('nom');?>
	<div class='pure-control-group'>
      <label>Nom</label>
      <input type='text' name='nom' placeholder='nom de groupe'  value='<?php echo valeur('nom');?>'/>
  </div>

	<?php affiche_erreur('email');?>
	<div class='pure-control-group'>
      <label>email</label>
      <input type='text' name='email' placeholder="mail valide" value='<?php echo valeur('email');?>'/>
  </div>
	<?php affiche_erreur('pass');?>
  <div class="pure-control-group">
      <label>password</label>
			<!-- on ne réinjecte pas le mot de passe -->
			<input type='password' name='pass' placeholder="mot de passe" />
  </div>
	<?php affiche_erreur('Dept');?>
  <div class='pure-control-group'>
      <label>Département</label>
      <select name="Dept">
        <option value="">--</option>
        <?php
        while($ligne=$depts->fetch(PDO::FETCH_NUM)){
					//on sélectionne l'option qui a déjà été choisie par l'utilsateur
					$selected = $ligne[0] == valeur('Dept') ? "selected":"";
				  echo "<option $selected value='$ligne[0]'>$ligne[1]</option>";
				}
        ?>
      </select>
    </div>
    <datalist id="annees">
    <?php
    //quelques valeurs suggérées à l'utilisateur
    for($i=Date('Y');$i>Date('Y')-10;$i--){
		  echo "<option value='$i'>";

		}
    ?>
    </datalist>
		<?php affiche_erreur('annee');?>
    <div class="pure-control-group">
        <label>Année de création</label>
        <input name='annee' type='number' list='annees' name='annee' min="1900" max="<?php echo Date('Y');?>" value="<?php echo valeur('annee');?>"/>
    </div>

		<?php affiche_erreur('web');?>
    <div class="pure-control-group">
        <label>site web</label>
        <input type='text' name='web' placeholder="https://" value="<?php echo valeur('web');?>"/>
    </div>

		<?php affiche_erreur('presentation');?>
    <div class="pure-control-group">
        <label>presentation</label>
        <textarea cols='50' rows='5' name='presentation' placeholder="Présentation courte du groupe"><?php echo valeur('presentation');?></textarea>
    </div>


		<?php affiche_erreur('scene');?>
        <?php
        while($ligne=$scenes->fetch(PDO::FETCH_NUM)){

					//on sélectionne l'option qui a déjà été choisie par l'utilsateur
					$checked = $ligne[0] == valeur('scene') ? "checked":"";


        echo "<div class='pure-controls'>";
        echo "<label><input $checked type='radio' name='scene' value='$ligne[0]'> $ligne[1]</label>";
        echo "</div>";
      }
      ?>

			<?php affiche_erreur('Accepte');?>
      <div class='pure-controls'>
				<?php
				$checked = !empty(valeur('Accepte')) ? "checked":"";
				?>
        <label><input type='checkbox' <?php echo $checked; ?> name='Accepte' value='oui'> J'accepte les conditions d'utilisation</label>
</div>

          <div class='pure-controls'>
          <label><input type='submit'</label>
          </div>


  </div>
</form>
</div>
</div>
</body>
</html>
